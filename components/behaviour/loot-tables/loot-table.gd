@icon("res://components/behaviour/loot-tables/loot_table.svg")
class_name LootTable extends Node

enum ITEM_RARITY { COMMON, UNCOMMON, RARE, LEGENDARY, UNIQUE } ## Expand here as to adjust it to your game requirements

## The available items that will be used on a roll
@export var available_items: Array[LootTableItem] = []


func _init(items: Array[LootTableItem] = []):
	add_items(items)


func filter_by_rarities(rarities: Array[ITEM_RARITY] = []) -> Array[LootTableItem]:
	return available_items.filter(func(item: LootTableItem): item.rarity in rarities)


func filter_by_rarity(rarity: ITEM_RARITY) -> Array[LootTableItem]:
	return available_items.filter(func(item: LootTableItem): item.rarity == rarity)


func add_items(items: Array[LootTableItem] = []) -> void:
	available_items.append_array(items)


func add_item(item: LootTableItem) -> void:
	available_items.append(item)


func remove_items(items: Array[LootTableItem] = []) -> void:
	available_items = available_items.filter(func(item: LootTableItem): return not item in items)


func remove_item(item: LootTableItem) -> void:
	available_items.erase(item)


func remove_items_by_id(item_ids: Array[StringName] = []) -> void:
	available_items = available_items.filter(func(item: LootTableItem): return not item.id in item_ids)


func remove_item_by_id(item_id: StringName) -> void:
	available_items  = available_items.filter(func(item: LootTableItem): return not item.id == item_id)
