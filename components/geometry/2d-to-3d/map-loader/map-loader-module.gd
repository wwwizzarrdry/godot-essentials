@tool
class_name MapLoaderModule extends Node3D

enum MODULE_TYPES {
	PLANE,
	DEPTH
}

## Where 'x' is width, 'y' is height and 'z' is depth
@export var size := Vector3(1, 3.5, 0.1)
@export var type := MODULE_TYPES.DEPTH
@export var render_walls := true:
	set(value):
		render_walls = value
		if is_node_ready():
			for wall in [north_wall, south_wall, west_wall, east_wall]:
				if wall:
					wall.visible = render_walls
			
@export var render_ceil := true:
	set(value):
		render_ceil = value
		
		if is_node_ready() and ceil:
			ceil.visible = render_ceil
		
@export var render_floor := true:
	set(value):
		render_floor = value
		
		if is_node_ready() and floor:
			floor.visible = render_floor

@onready var floor: MeshInstance3D = $Floor
@onready var ceil: MeshInstance3D = $Ceil
@onready var north_wall: MeshInstance3D = $NorthWall
@onready var south_wall: MeshInstance3D = $SouthWall
@onready var east_wall: MeshInstance3D = $EastWall
@onready var west_wall: MeshInstance3D = $WestWall

@onready var vector_to_wall := {
		Vector2.UP: north_wall,
		Vector2.DOWN: south_wall,
		Vector2.RIGHT: east_wall,
		Vector2.LEFT: west_wall
}

const GROUP_NAME = "map-loader-module"


func _enter_tree():
	add_to_group(GROUP_NAME)
	name = "MapLoaderModule%d" % get_tree().get_nodes_in_group(GROUP_NAME).size()


func _ready():
	prepare_visibility()

## Dictionary<Vector2, bool>
func update_faces(neighbours: Dictionary) -> void:
	if neighbours.is_empty():
		return
	
	for neighbour_direction: Vector2 in neighbours.keys():
		if vector_to_wall.has(neighbour_direction) and neighbours[neighbour_direction]:
			vector_to_wall[neighbour_direction].hide()
			
	

func prepare_visibility():
	floor.visible = render_floor
	ceil.visible = render_ceil
	
	for wall in [north_wall, south_wall, west_wall, east_wall]:
		wall.visible = render_walls
	

func available_meshes() -> Array[MeshInstance3D]:
	return [floor, ceil, north_wall, south_wall, east_wall, west_wall]


func visible_meshes() -> Array[MeshInstance3D]:
	return available_meshes().filter(func(mesh: MeshInstance3D): return mesh.visible)
	
