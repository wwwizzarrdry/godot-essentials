@tool
class_name MapLoader extends Node


@export_file("*.tscn") var map := ""
@export var generate := false:
	set(value):
		generate = false
		
		if Engine.is_editor_hint() and FileManager.filepath_is_valid(map):
			generate_map();
		
@export var clear := false:
	set(value):
		clear = false
		
		if Engine.is_editor_hint() and generated_map_root.get_child_count() > 0:
			clear_generated_map();
		
@export_group("Display")
@export var map_module_size := Vector3(1, 3.5, 0.1)
@export var render_walls := true
@export var render_ceil := true
@export var render_floor := true
@export_group("Mesh generation")
@export var merge_meshes := true
@export var generate_floor_collisions := true
@export var generate_ceil_collisions := true
@export var generate_walls_collisions := true

@onready var generated_map_root := $GeneratedMap

## Dictionary<String, PackedScene>
static var cached_scenes := {}


func generate_map():
	if FileManager.filepath_is_valid(map):
		clear_generated_map()
		
		var tilemap_scene = load(map) as PackedScene
		var tilemap := tilemap_scene.instantiate() as TileMap
		
		var layers := tilemap.get_layers_count()
		var single_meshes := {}
		
		var tilemap_root_node = Node3D.new()
		tilemap_root_node.name = tilemap.name
		generated_map_root.add_child(tilemap_root_node)
		NodeWizard.set_owner_to_edited_scene_root(tilemap_root_node)
		
		for layer in layers:
			var cells := tilemap.get_used_cells(layer)
			
			for cell: Vector2i in cells:
				var tile_data = tilemap.get_cell_tile_data(layer, cell)
				var map_module_scene = obtain_scene_from_custom_tile_data(tile_data)
				
				if map_module_scene:
					var map_module = map_module_scene.instantiate() as MapLoaderModule
					
					map_module.render_ceil = render_ceil
					map_module.render_floor = render_floor
					map_module.render_walls = render_walls
					
					tilemap_root_node.add_child(map_module)
					NodeWizard.set_owner_to_edited_scene_root(map_module)
					
					var x_coord = cell.x * map_module_size.x
					var z_coord = cell.y * map_module_size.x
					
					map_module.translate(Vector3(x_coord, 0, z_coord))
					map_module.update_faces(get_cell_neighbours(cells, cell))
					
					if merge_meshes:
						## Dictionary<String, SingleMeshMerged>
						
						for mesh: MeshInstance3D in map_module.visible_meshes():
							var type := _key_type_from_mesh(mesh)
							
							if not single_meshes.has(type):
								single_meshes[type] = SingleMeshMerged.new(type)
							
							single_meshes[type].merge_mesh(mesh)								
	
		if merge_meshes:
			for single_mesh_merged: SingleMeshMerged in single_meshes.values():
				single_mesh_merged.add_to_scene_tree(tilemap_root_node, _generate_collisions_on_type(single_mesh_merged.type))
	
			single_meshes.clear()
			NodeWizard.remove_and_queue_free_children(tilemap_root_node, [MeshInstance3D.new().get_class()])


func obtain_scene_from_custom_tile_data(data: TileData) -> PackedScene:
	var scene_path: String = data.get_custom_data_by_layer_id(0)
	
	if cached_scenes.has(scene_path):
		return cached_scenes[scene_path]
	else:
		if scene_path.is_absolute_path() and ResourceLoader.exists(scene_path):
			cached_scenes[scene_path] = load(scene_path) as PackedScene
	
	return null


func get_cell_neighbours(cells: Array[Vector2i], cell: Vector2i) -> Dictionary:
	return {
		Vector2.UP: cells.has(cell + Vector2i.UP),
		Vector2.DOWN: cells.has(cell + Vector2i.DOWN),
		Vector2.LEFT: cells.has(cell + Vector2i.LEFT),
		Vector2.RIGHT: cells.has(cell + Vector2i.RIGHT),
	}


func clear_generated_map():
	NodeWizard.queue_free_children(generated_map_root)
	

	
func _generate_collisions_on_type(type: String) -> bool:
	match type:
		"floor":
			return generate_floor_collisions
		"ceil":
			return generate_ceil_collisions
		"wall":
			return generate_walls_collisions
		_:
			return false


func _key_type_from_mesh(mesh: MeshInstance3D) -> String:
	var mesh_name = mesh.name.to_lower()
	
	if mesh_name.contains("wall"):
		return "wall"
	else:
		return mesh_name



class SingleMeshMerged:
	var mesh_name := ""
	var array_mesh: ArrayMesh = ArrayMesh.new()
	var surface_tool := SurfaceTool.new()
	var type:= ""
	
	func _init(_type: String):
		type = _type.to_lower()
		mesh_name = "%sSingleMesh" % type.capitalize()
	
	
	func merge_mesh(mesh_to_merge: MeshInstance3D) -> void:
		if mesh_to_merge.mesh:
			surface_tool.append_from(mesh_to_merge.mesh, 0, mesh_to_merge.global_transform)
			surface_tool.commit(array_mesh)


	func add_to_scene_tree(parent: Node3D, generate_collisions: bool = true):
		var result_mesh = MeshInstance3D.new()
		result_mesh.name = mesh_name
		result_mesh.mesh = array_mesh
		parent.add_child(result_mesh)

		NodeWizard.set_owner_to_edited_scene_root(result_mesh)
		
		if generate_collisions:
			var static_body = StaticBody3D.new()
			static_body.name = "%sStaticBody" % mesh_name
			
			var collision = CollisionShape3D.new()
			collision.name = "%sCollision" % mesh_name
			collision.shape = result_mesh.mesh.create_trimesh_shape()
			
			static_body.add_child(collision)
			result_mesh.add_child(static_body)
			
			NodeWizard.set_owner_to_edited_scene_root(static_body)
			NodeWizard.set_owner_to_edited_scene_root(collision)

