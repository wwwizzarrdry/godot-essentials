extends Node

var DEBUG_MODE := OS.is_debug_build()

## This is use as a bridge to let know what's the next scene, useful for scene manager and loading screen
var next_scene_path: String

